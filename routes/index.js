'use strict';

var express = require('express');
var router = express.Router();

var validator = require('validator');

var db = require('./database');
var mailer = require('./mailer');

/* ランダムな文字列を生成する */
var getRandomStr = function(num) {
	var strList = 'abcdefgeijklmnopqrstuvwxyz1234567890',
			str = '',
			i;

	for (i = 1; i <= num; i++) {
		str += strList[Math.floor(Math.random() * strList.length)];
	}

	return str;
};

/* オブジェクトの各プロパティの文字列をサニタイズ */
var sanitize = function(obj) {
	var newObj = {};

	for (var i in obj){
		newObj[i] = validator.escape(obj[i]);
	}

	return newObj;
};

///*** ページ取得 ***///

/* GET home page. */
router.get('/', function(req, res) {
	res.render('index');
});

/* GET ログインページ */
router.get('/login', function(req, res) {
	res.render('login');
});

/* POST ログイン完了ページ */
router.post('/login-complete', function(req, res) {
	// ログイン情報を元にユーザー情報を取得する
	db.getUserInfo(req.body, function(err, rows) {
		if (err) {
			// 取得出来なかった
			res.render('error', {message: '取得出来なかった'});
		} else if (rows.length === 0 || rows[0].state === 1 || rows[0].password !== req.body.password) {
			// IDが存在しなかった||仮登録状態だった||登録情報のパスワードと入力パスワードが一致しなかった
			res.render('error', {message: 'ID・パスワードが違うか、本登録完了していないアカウントです！'});
		} else {
			// ログイン完了
			res.render('login-complete', {name: rows[0].name});
		}
	});
});

/* GET 新規会員登録ページ */
router.get('/register', function(req, res) {
	res.render('register');
});

/* POST 仮登録完了ページ */
router.post('/signin', function(req, res) {
	// 入力内容が不足していた場合エラーページへ
	if (!req.body.id || !req.body.password || !req.body.name || !req.body.address) {
		res.render('error', {message: 'ちゃんと入力するべし'});
	}

	// 登録情報にランダムな文字列を追加
	var query = req.body;
			query.registURL = getRandomStr(20); 

	// テーブルに登録情報を入力する
	db.insertValues(query, function() {
		// メーラーの準備
		mailer.init({
				service: 'Gmail',
				auth: {
					user: 'signintestaddress@gmail.com',
					pass: 'masanor1'
				}
		});

		// 仮登録のメール送信
		mailer.send(query.address, 'http://localhost:3000/register-complete?regist=' + query.registURL);
	});

	res.render('signin', query);
});

/* GET 本登録完了ページ */
router.get('/register-complete', function(req, res) {
	// 登録URLに一致するアカウントのステータスを更新する
	db.updateState(req.query.regist, function() {
		res.render('register-complete');
	});
});

module.exports = router;
