var mysql = require('mysql');

var database = {
	// MySQLの設定
	config: {
		host: 'localhost',
		port: 3306,
		user: 'member',
		password: '',
		database:'member'
	},

	/* テーブルにアカウントデータを追加する */
	insertValues: function(options, callback) {
		// データベースに接続
		var client = mysql.createConnection(this.config);

		// データ追加用クエリ作成
		var queryStr = 'insert into memberTB(id, password, name, state, address, registURL) values("' + options.id + '", "' + options.password + '", "' + options.name + '", 1, "' + options.address + '", "' + options.registURL +'")';

		// 仮登録としてデータをテーブルに追加
		client.query(queryStr, function (err, rows) {
			if (err) {
				console.log('\u001b[31m' + 'クエリ発行時のエラー');
				console.log(err);
			} else {
				callback();
			}

			client.end();
		});
	},

	/* 登録URLに一致するアカウントのステータスを更新する */
	updateState: function(url, callback) {
		// データベースに接続
		var client = mysql.createConnection(this.config);
		var queryUpdateState = 'update memberTB set state=0 where registURL="' + url + '"';

		// テーブルのstateを更新して本登録完了とする
		client.query(queryUpdateState, function(err, rows) {
			if (err) {
				console.log('\u001b[31m' + 'テーブル更新時のエラー');
				console.log(err);
			} else {
				callback();
			}

			client.end();
		});
	},

	/* IDからアカウント情報を検索する */
	getUserInfo: function(options, callback) {
		var client = mysql.createConnection(this.config);
		var query = 'select * from memberTB where id="' + options.id + '"';

		client.query(query, function(err, rows) {
			callback(err, rows);
			client.end();
		});
	}
};

module.exports = database;