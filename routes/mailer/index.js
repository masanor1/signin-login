var nodemailer = require('nodemailer');

var sendMail = {
	// 送信に使用するアカウント設定
	transport: null,

	// 送信内容
	sendOptions: {
		from: 'じゃがいも <signintestaddress@gmail.com>',
		to: null,
		subject: '仮登録が完了しました！',
		text: '',
		html: null
	},

	/* 初期処理 */
	init: function(options) {
		sendMail.transport = nodemailer.createTransport('SMTP', options);
	},

	/* 仮登録メールを送信する */
	send: function(toAddress, signinUrl) {
		var self = this;
		self.sendOptions.to = toAddress;
		self.sendOptions.html = '<h2>仮登録完了です！</h2><br><h3>以下のアドレスにアクセスして本登録完了です</h3><br><p>' + signinUrl + '</p>';

		self.transport.sendMail(self.sendOptions, function(err, res){
			if(err){
				console.log(err);
			}else{
				console.log('Message sent: ' + res.message);
			}

			self.transport.close();
		});
	}
};

module.exports = sendMail;