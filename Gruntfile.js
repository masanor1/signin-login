module.exports = function(grunt) {
	//グラントタスクの設定
	grunt.initConfig({
		browserify: {
			dist: {
				files: {
				  'public/js/main.js': ['develop/js/main.js']
				},
				options: {
					transform: ['brfs']
				}
			}
		},
		//watchの設定
		watch: {
			// css: {
			// 	files: ['develop/scss/*.scss'],
			// 	tasks: ['sass']
			// },
			js: {
				files: ['develop/js/*.js'],
				tasks: ['browserify']
			},
		}
	});
	//プラグインの読み込み
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-browserify');
};
